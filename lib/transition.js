/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to define a transition of FSM.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */

/** @typedef {import('./task')} Task */
/**
 * The path from state one to state two.
 * @public
 */
class Transition {
  /**
   * Create a transition.
   * @param {string} fromState the code from which state
   * @param {string} toState the code to which state
   * @param {string} code the code of this transition must be unique to it's fromState
   * @since 0.4 definition changed
   */
  constructor(fromState, toState, code) {
    /**
     * The code of the state this transition comes from.
     * @type {string}
     */
    this.fromState = fromState;
    /**
     * The code of the state this transition goes to.
     * @type {string}
     */
    this.toState = toState;
    /** 
     * Must be unique string to it's fromState.
     * @type {string}
     * @private
     */
    this._code = code;
  }
  /**
   * Get the code of this transition, must be a constant value and unique to it's fromState.
   * @since 0.4 definition changed
   */
  get code() {
    return this._code ? this._code : this.constructor.name;
  }
  /**
   * Run specific logic of this transition.
   * @callback onTransition
   * @param {Task} task the task to be executed
   * @returns {void}
   */
  onTransition(task) {
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Transition;