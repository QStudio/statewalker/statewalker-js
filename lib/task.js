/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to define a task.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.1
 */

/** 
 * The status of a task.
 * @readonly
 * @enum {number}
 * @public
 */
const Status = {
  /**
   * Task is inited.
   * Task will be keeped in this status before entry state execution process completed.
   */
  INIT: 1,
  /** Task is processing. */
  PROCESSING: 2,
  /**
   * Task is sleeping.
   * @since 0.3
   */
  SLEEPING: 3,
  /** Task is suspended. */
  SUSPENDED: 4,
  /** Task is completed. */
  COMPLETED: 5,
  /** Task was end with error. */
  ERROR: 6
}
Object.freeze(Status);

/**
 * A task to be execute.
 * @public
 */
class Task {
  /**
   * Create a task.
   * @param {*} content the content
   * @param {string} state the initial state code
   * @package
   */
  constructor(content, state) {
    /** The content of this task. */
    this.content = content;
    /**
     * The code of current state.
     * @type {string}
     */
    this.state = state;
    /**
     * @type {Status}
     */
    this.status = Status.INIT;
    /**
     * The code of previous state.
     * @type {string}
     */
    this.previousState = undefined;
    /**
     * The code of upcoming transition.
     * @type {string}
     */
    this.transition = undefined;
    /**
     * The action tracing data.
     * @type {Action[]}
     */
    this.actions = [];
    /**
     * The cause makes this task status be error.
     * @type {Error}
     */
    this.errorCause = undefined;
    /**
     * The time this task should wake up.
     * @type {number}
     * @since 0.3
     */
    this.wakeupTime = undefined;
  }
  /**
   * Start a execution process.
   * @package
   */
  executionStart() {
    this.actions.length = 0;
    this.errorCause = null;
    this.wakeupTime = null;
    switch (this.status) {
      case Status.SUSPENDED:
      case Status.SLEEPING:
      case Status.ERROR:
        this.status = Status.PROCESSING;
        break;
      default:
    }
  }
  /**
   * End a execution process.
   * @param {string} state current state code
   * @param {string=} transition the upcoming transtion code, not existed if no transition.
   * @package
   */
  executionEnd(state, transition) {
    if (this.status === Status.INIT) {
      this.status = Status.PROCESSING;
    } else {
      this.previousState = this.state;
    }
    this.state = state;
    this.transition = transition;
  }
  /**
   * End a execution process - the task is suspended.
   * @param {string} state current state code
   * @package
   * @since 0.3
   */
  executionSuspend(state) {
    this.executionEnd(state);
    this.status = Status.SUSPENDED;
  }
  /**
   * End a execution process - the task goes to sleep.
   * @param {string} state current state code
   * @param {number} sleepTime sleep time in milliseconds
   * @package
   * @since 0.3
   */
  executionSleep(state, sleepTime) {
    this.executionEnd(state);
    this.wakeupTime = Date.now() + sleepTime;
    this.status = Status.SLEEPING;
  }
  /**
   * End a execution process - the task raised an error.
   * @param {Error} cause the reason
   * @package
   */
  executionError(cause) {
    this.status = Status.ERROR;
    this.errorCause = cause;
  }
  /**
   * End a execution process - the task is completed.
   * @param {string} state current state code
   * @package
   * @since 0.3 renamed from complete
   */
  executionComplete(state) {
    this.previousState = this.state;
    this.state = state;
    this.status = Status.COMPLETED;
    this.transition = undefined;
  }
  /**
   * Record new action.
   * @param {string} name action name
   * @param {string} provider action provider name
   * @package
   */
  actionStart(name, provider) {
    let action = new Action(name, provider);
    this.actions.push(action);
  }
  /**
   * End latest action if not end.
   * @package
   */
  actionEnd() {
    this.actions[this.actions.length - 1].end();
  }
}

/**
 * The action records one step tracing data of a task single execution.
 * @public
 */
class Action {
  /**
   * Create an action.
   * @param {string} name action name
   * @param {string} provider action provider name
   * @private
   */
  constructor(name, provider) {
    /**
     * Action name.
     * The method name such as onState.
     * @type {string}
     */
    this.name = name;
    /**
     * Provider name. The action provided by which state or transition.
     * It is the code of state or transition.
     * @type {string}
     */
    this.provider = provider;
    /**
     * Action start time.
     * @type {number}
     */
    this.startTime = Date.now();
    /**
     * Action end time.
     * @type {number}
     */
    this.endTime = undefined;
  }
  /**
   * Record end time.
   * @private
   */
  end() {
    this.endTime = Date.now();
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Task;
module.exports.Status = Status;
module.exports.Action = Action;
