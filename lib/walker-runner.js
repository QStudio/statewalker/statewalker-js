/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to define a walker runner which can run walker automatically and asynchronously.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.2
 */

/**
 * Module dependencies.
 * @private
 */
/** @typedef {import('./task')} Task */
const Task = require('./task');
/** 
 * @typedef {import('./walker')} Walker 
 */
const Walker = require('./walker');

/** Sleep time per milliseconds. */
const SLEEP_TIME = 20;

/**
 * The runner runs walkers automatically.
 * @public
 */
class WalkerRunner {
  /**
   * Create a new runner with specific amount of walkers.
   * @param {Walker} walker the base walker using in the thread
   * @package
   */
  constructor(walker) {
    /**
     * @private
     */
    this.walker = walker;
    /**
     * @private
     */
    this.started = false;
    /**
     * @private
     */
    this.running = false;
  }
  /**
   * Add a new task by content and initial state.
   * @param {*} content the content of the task
   * @param {string} state the initial state of the task
   * @returns {Task} the task added
   */
  assignTask(content, state) {
    return this.walker.addTask(content, state);
  }
  /**
   * Core Logic to ask walker doTask.
   * @private
   */
  run() {
    const working = this.walker.hasTask();
    if (working) {
      this.walker.doTask();
    }
    if (this.started && this.walker._tasks.length !== 0) {
      setImmediate(this.run.bind(this));
    } else if (this.started) {
      setTimeout(this.run.bind(this), SLEEP_TIME);
    } else {
      this.running = false;
    }
  }
  /**
   * Start runner.
   * @returns {boolean} success start
   */
  start() {
    if (this.started) {
      return false;
    }
    this.started = true;
    setImmediate(this.run.bind(this));
    this.running = true;
    return true;
  }
  /**
   * Stop runner.
   * @returns {boolean} success stop.
   */
  stop() {
    if (!this.started) {
      return false;
    }
    this.started = false;
    return true;
  }
  /**
   * Has task or not.
   * @returns {boolean} true if tasks not completed
   */
  hasTask() {
    return this.walker.hasTask();
  }
  /**
   * This runner is running or not.
   * @returns {boolean} running or not
   */
  isRunning() {
    return this.running;
  }
  /**
   * This runner is busy or not.
   * Busy means the runner is running, has tasks to be completed.
   * @returns {boolean} busy or not
   */
  isBusy() {
    return this.running && this.walker.hasTask();
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = WalkerRunner;