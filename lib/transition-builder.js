/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to build a transtion of FSM.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.3 refactor from transition.js
 */

/**
 * Module dependencies.
 * @private
 */
/** 
 * @typedef {import('./transition')} Transition
 * @typedef {(task: Task) => void} onTransition
 */
const Transition = require('./transition');

/**
 * The builder of transition.
 * @public
 */
class Builder {
  /**
   * Set the code for the transition which must be an unique constant string to it's from state.
   * @param {string} code the code of the transition
   * @returns {Builder} this builder
   * @since 0.4 definition changed
   */
  setCode(code) {
    /** 
     * @type {string}
     * @private
     */
    this.code = code;
    return this;
  }
  /**
   * Set the code for the state which the transition comes from.
   * @param {string} fromState the code from which state
   * @returns {Builder} this builder
   */
  setFromState(fromState) {
    /**
     * @type {string}
     * @private
     */
    this.fromState = fromState;
    return this;
  }
  /**
   * Set the code for the state which the transition goes to.
   * @param {string} toState the code to which state
   * @returns {Builder} this builder
   */
  setToState(toState) {
    /**
     * @type {string}
     * @private
     */
    this.toState = toState;
    return this;
  }
  /**
   * Set the onTransition method for the transition.
   * @param {onTransition} onTransition the onTransition logic of the transition
   * @returns {Builder} this builder
   */
  setOnTransition(onTransition) {
    /**
     * @type {onTransition=}
     * @private
     */
    this.onTransition = onTransition;
    return this;
  }
  /**
   * Build the transition.
   * @returns {Transition} the transition
   */
  build() {
    let transition = new Transition(this.fromState, this.toState, this.code);
    if (typeof this.onTransition === 'function') {
      transition.onTransition = this.onTransition;
    }
    return transition;
  }
  /**
   * Get a new builder.
   * @returns {Builder} a new builder
   */
  static get() {
    return new Builder();
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Builder;