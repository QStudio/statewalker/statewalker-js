/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to build a state of FSM.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3 refactor from state.js
 */

/**
 * Module dependencies.
 * @private
 */
/** 
 * @typedef {import('./state')} State
 * @typedef {import('./state').Result} Result
 * @typedef {(task: Task) => Result} onState
 * @typedef {(task: Task) => void} onEnterState
 * @typedef {(task: Task) => void} onExitState
 */
const State = require('./state');

/**
 * The builder of state.
 * @public
 */
class Builder {
  /**
   * Set the code for the state which must be an unique constant string in the application.
   * @param {string} code the code of the state
   * @returns {Builder} this builder
   */
  setCode(code) {
    /**
     * @type {string}
     * @private
     */
    this.code = code;
    return this;
  }
  /**
   * Set the onEnterState method for the state.
   * @param {onEnterState} onEnterState the onEnterState logic of the state
   * @returns {Builder} this builder
   */
  setOnEnterState(onEnterState) {
    /**
     * @type {onEnterState=}
     * @private
     */
    this.onEnterState = onEnterState;
    return this;
  }
  /**
   * Set the onExitState method for the state.
   * @param {onExitState} onExitState the onExitState logic of the state
   * @returns {Builder} this builder
   */
  setOnExitState(onExitState) {
    /**
     * @type {onExitState=}
     * @private
     */
    this.onExitState = onExitState;
    return this;
  }
  /**
   * Set the onState method for the state.
   * @param {onState} onState the onState logic of the state
   * @returns {Builder} this builder
   */
  setOnState(onState) {
    /**
     * @type {onState}
     * @private
     */
    this.onState = onState;
    return this;
  }
  /**
   * Build the state.
   * @returns {State} the state
   */
  build() {
    let state = new State(this.code);
    if (typeof this.onEnterState === 'function') {
      state.onEnterState = this.onEnterState;
    }
    if (typeof this.onExitState === 'function') {
      state.onExitState = this.onExitState;
    }
    if (typeof this.onState === 'function') {
      state.onState = this.onState;
    }
    return state;
  }
  /**
   * Get a new builder.
   * @returns {Builder} a new builder
   */
  static get() {
    return new Builder();
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Builder;