/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to build a walker.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.3 refactor from walker.js
 */

/**
 * Module dependencies.
 * @private
 */
/** 
 * @typedef {import('./state')} State
 * @typedef {import('./state').Result} Result
 * @typedef {(task: Task) => Result} onState
 * @typedef {(task: Task) => void} onEnterState
 * @typedef {(task: Task) => void} onExitState
 */
const State = require('./state');
/**
 * @typedef {import('./state-builder')} StateBuilder
 */
const StateBuilder = require('./state-builder');
/** 
 * @typedef {import('./transition')} Transition
 * @typedef {(task: Task) => void} onTransition
 */
const Transition = require('./transition');
/**
 * @typedef {import('./transition-builder')} TransitionBuilder
 */
const TransitionBuilder = require('./transition-builder');
/**
 * @typedef {import('./walker')} Walker
 */
const Walker = require('./walker');

/**
 * The builder of walker.
 * @public
 */
class Builder {
  /**
   * Create a builder.
   * @private
   */
  constructor() {
    /**
     * @type {State[]} 
     */
    this.states = [];
    /**
     * @type {Transition[]}
     */
    this.transitions = [];
    /**
     * @type {onTaskComplete[]}
     * @since 0.2
     */
    this.onTaskComplete = [];
    /**
     * @type {onTaskError[]}
     * @since 0.2
     */
    this.onTaskError = [];
    /**
     * @type {onTaskSuspend[]}
     * @since 0.2
     */
    this.onTaskSuspend = [];
    /**
     * @type {onTaskSleep[]}
     * @since 0.3
     */
    this.onTaskSleep = [];
    /**
     * @type {boolean}
     */
    this.enableActionTracing = false;
  }
  /**
   * Add a state for the walker.
   * @param {string} code the code of the state
   * @param {onState} onState the onState logic of the state
   * @param {onEnterState=} onEnterState the onEnterState logic of the state
   * @param {onExitState=} onExitState the onExitState logic of the state
   * @since 0.2
   */
  addState(code, onState, onEnterState, onExitState) {
    this.states.push(StateBuilder.get()
      .setCode(code)
      .setOnState(onState)
      .setOnEnterState(onEnterState)
      .setOnExitState(onExitState)
      .build()
    );
    return this;
  }
  /**
   * Add states for the walker.
   * @param  {...State} states the states the walker should know
   * @returns {Builder} this builder
   */
  addStates(...states) {
    this.states.push(...states);
    return this;
  }
  /**
   * Add a transition for the walker.
   * @param {string} code the transition code
   * @param {string} fromState the code from which state
   * @param {string} toState the code to which state
   * @param {onTransition=} onTransition the onTransition logic of the transition
   * @returns {Builder} this builder
   * @since 0.4 code changes to required
   * @since 0.2
   */
  addTransition(code, fromState, toState, onTransition) {
    this.transitions.push(TransitionBuilder.get()
      .setCode(code)
      .setFromState(fromState)
      .setToState(toState)
      .setOnTransition(onTransition)
      .build()
    );
    return this;
  }
  /**
   * Add transitions for the walker.
   * @param  {...Transition} transitions the transitions the walker should know
   * @returns {Builder} this builder
   */
  addTransitions(...transitions) {
    this.transitions.push(...transitions);
    return this;
  }
  /**
   * Add onTaskComplete callbacks for the walker.
   * @param  {...onTaskComplete} onTaskComplete the callbacks invoked when a task completed
   * @returns {Builder} this builder
   * @since 0.2
   */
  addOnTaskComplete(...onTaskComplete) {
    add(this.onTaskComplete, onTaskComplete);
    return this;
  }
  /**
   * Add onTaskError callbacks for the walker.
   * @param  {...onTaskError} onTaskError the callbacks invoked when a task raised an error
   * @returns {Builder} this builder
   * @since 0.2
   */
  addOnTaskError(...onTaskError) {
    add(this.onTaskError, onTaskError);
    return this;
  }
  /**
   * Add onTaskSuspend callbacks for the walker.
   * @param  {...onTaskSuspend} onTaskSuspend the callbacks invoked when a task suspended
   * @returns {Builder} this builder
   * @since 0.2
   */
  addOnTaskSuspend(...onTaskSuspend) {
    add(this.onTaskSuspend, onTaskSuspend);
    return this;
  }
  /**
   * Add onTaskSleep callbacks for the walker.
   * @param  {...onTaskSleep} onTaskSleep the callbacks invoked when a task went to sleep
   * @returns {Builder} this builder
   * @since 0.3
   */
  addOnTaskSleep(...onTaskSleep) {
    add(this.onTaskSleep, onTaskSleep);
    return this;
  }
  /**
   * Set enable action tracing or not for the walker.
   * @param {boolean} enableActionTracing enable action tracing
   * @returns {Builder} this builder
   */
  setEnableActionTracing(enableActionTracing) {
    this.enableActionTracing = enableActionTracing;
    return this;
  }
  /**
   * Build the walker.
   * @returns {Walker} the walker
   */
  build() {
    let walker = new Walker(this.enableActionTracing);
    for (const s of this.states) {
      walker.states.set(s.code, s);
    }
    for (const t of this.transitions) {
      let tMap = walker.transitions.get(t.fromState);
      if (!tMap) {
        tMap = new Map();
        walker.transitions.set(t.fromState, tMap);
      }
      tMap.set(t.code, t);
    }
    walker.onTaskComplete.push(...this.onTaskComplete);
    walker.onTaskError.push(...this.onTaskError);
    walker.onTaskSuspend.push(...this.onTaskSuspend);
    walker.onTaskSleep.push(...this.onTaskSleep);
    return walker;
  }
  /**
   * Get a new builder.
   * @returns {Builder} a new builder
   */
  static get() {
    return new Builder();
  }
}

/**
 * Add elements to array and make sure no duplication.
 * @param {any[]} array target array
 * @param {any[]} elements the elements
 * @private
 * @since 0.2
 */
function add(array, elements) {
  for (let e of elements) {
    if (!array.includes(e)) {
      array.push(e);
    }
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Builder;