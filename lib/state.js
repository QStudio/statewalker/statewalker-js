/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to define a state of FSM.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 * @since 0.3 refactor the builder to state-builder.js
 */

/** 
 * The feedback for walker after onState executed.
 * @readonly
 * @enum {number}
 * @public
 */
const Feedback = {
  /** Continue doing task. */
  CONTINUE: 1,
  /** Suspend task. */
  SUSPEND: 2,
  /** Task sleep. */
  SLEEP: 3,
  /** Complete task. */
  COMPLETE: 4
}
Object.freeze(Feedback);

/** @typedef {import('./task')} Task */
/**
 * A step of excuting task.
 * The life cycle method sequence is onEnterState -> onState -> onExitState.
 * @public
 */
class State {
  /**
   * Create a state.
   * @param {string} code the code of this state
   */
  constructor(code) {
    /** 
     * Must be unique string in the application.
     * @type {string}
     */
    this.code = code ? code : this.constructor.name;
  }
  /**
   * Run specific logic on enter this state.
   * @callback onEnterState
   * @param {Task} task the task to be executed
   * @returns {void}
   */
  onEnterState(task) {
  }
  /**
   * Run specific logic of this state.
   * @callback onState
   * @param {Task} task the task to be executed
   * @returns {Result} the result after running logic
   */
  onState(task) {
    return Result.complete();
  }
  /**
   * Run specific logic on exit this state.
   * @callback onExitState
   * @param {Task} task the task to be executed
   * @returns {void}
   */
  onExitState(task) {
  }
}

/**
 * The result of running state logic.
 * @public
 */
class Result {
  /**
   * Create a result.
   * @param {Feedback} feedback the feedback
   * @param {string} exitCode the exit code
   * @param {number} sleepTime sleep time in milliseconds
   * @private
   */
  constructor(feedback, exitCode, sleepTime) {
    /** 
     * The feedback for walker.
     * @type {Feedback}
     */
    this.feedback = feedback;
    /** 
     * The exitCode for Walker to route to next state.
     * In current implementation, the exitCode should be the code of transitions from current state to next state.
     * @type {string}
     * @since 0.4 renamed to exitCode
     */
    this.exitCode = exitCode;
    /** 
     * Sleep time in milliseconds.
     * @type {number}
     * @since 0.3
     */
    this.sleepTime = sleepTime;
  }
  /**
   * Generate a result to exit this state.
   * @param {string} exitCode the exit code
   * @returns {Result} the result to exit this state
   * @since 0.4 nenamed to exit
   */
  static exit(exitCode) {
    return new Result(Feedback.CONTINUE, exitCode);
  }
  /**
   * Stay on the same state.
   * @returns {Result} the result to stay on the same state
   */
  static stay() {
    return Result.STAY;
  }
  /**
   * Suspend and stay on the same state.
   * This means the first state will be the suspended one when task resumed.
   * @returns {Result} the result to suspend
   * @since 0.3 renamed from stayAndWait
   */
  static suspend() {
    return Result.SUSPEND;
  }
  /**
   * Generate a completed result.
   * @returns {Result} the result which the task is completed
   */
  static complete() {
    return Result.COMPLETE;
  }
  /**
   * Sleep for the specific time period. If sleepTime less then 0 will be set to 0.
   * @param {number} sleepTime sleep time in milliseconds
   * @returns {Result} the result to sleep
   */
  static sleep(sleepTime) {
    return new Result(Feedback.SLEEP, undefined, (sleepTime < 0 ? 0 : sleepTime));
  }
}
/**
 * The result for stay.
 * @private
 * @since 0.3
 */
Result.STAY = new Result(Feedback.CONTINUE);
/**
 * The result for suspend.
 * @private
 * @since 0.3
 */
Result.SUSPEND = new Result(Feedback.SUSPEND);
/**
 * The result for complete.
 * @private
 * @since 0.3
 */
Result.COMPLETE = new Result(Feedback.COMPLETE);

/**
 * Module exports.
 * @public
 */
module.exports = State;
module.exports.Result = Result;
module.exports.Feedback = Feedback;