/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to build a walker runner.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3 refactor from walker-runner.js
 */

/**
 * Module dependencies.
 * @private
 */
/**
 * @typedef {import('./state')} State
 * @typedef {import('./state').Result} Result
 * @typedef {(task: Task) => Result} onState
 * @typedef {(task: Task) => void} onEnterState
 * @typedef {(task: Task) => void} onExitState
 * @typedef {import('./transition')} Transition
 * @typedef {(task: Task) => void} onTransition
 * @typedef {import('./walker')} Walker
 */
const Walker = require('./walker');
/**
 * @typedef {import('./walker-builder')} WalkerBuilder
 */
const WalkerBuilder = require('./walker-builder');
/**
 * @typedef {import('./walker-runner')} WalkerRunner
 */
const WalkerRunner = require('./walker-runner');

/**
 * The builder of WalkerRunner.
 * @public
 */
class Builder {
  /**
   * Create a builder.
   * @private
   */
  constructor() {
    /**
     * @type {WalkerBuilder}
     * @private
     */
    this.wBuilder = WalkerBuilder.get();
  }
  /**
   * Add a state for the walker inside the runner.
   * @param {string} code the code of the state
   * @param {onState} onState the onState logic of the state
   * @param {onEnterState=} onEnterState the onEnterState logic of the state
   * @param {onExitState=} onExitState the onExitState logic of the state
   */
  addState(code, onState, onEnterState, onExitState) {
    this.wBuilder.addState(code, onState, onEnterState, onExitState);
    return this;
  }
  /**
   * Add states for the walker inside the runner.
   * @param  {...State} states the states the walker should know
   * @returns {Builder} this builder
   */
  addStates(...states) {
    this.wBuilder.addStates(...states);
    return this;
  }
  /**
   * Add a transition for the walker inside the runner.
   * @param {string} code the transition code
   * @param {string} fromState the code from which state
   * @param {string} toState the code to which state
   * @param {onTransition=} onTransition the onTransition logic of the transition
   * @returns {Builder} this builder
   * @since 0.4 code changes to required
   */
  addTransition(code, fromState, toState, onTransition) {
    this.wBuilder.addTransition(code, fromState, toState, onTransition);
    return this;
  }
  /**
   * Add transitions for the walker inside the runner.
   * @param  {...Transition} transitions the transitions the walker should know
   * @returns {Builder} this builder
   */
  addTransitions(...transitions) {
    this.wBuilder.addTransitions(...transitions);
    return this;
  }
  /**
   * Add onTaskComplete callbacks for the walker inside the runner.
   * @param  {...onTaskComplete} onTaskComplete the callbacks invoked when a task completed
   * @returns {Builder} this builder
   */
  addOnTaskComplete(...onTaskComplete) {
    this.wBuilder.addOnTaskComplete(...onTaskComplete);
    return this;
  }
  /**
   * Add onTaskError callbacks for the walker inside the runner.
   * @param  {...onTaskError} onTaskError the callbacks invoked when a task raised an error
   * @returns {Builder} this builder
   */
  addOnTaskError(...onTaskError) {
    this.wBuilder.addOnTaskError(...onTaskError);
    return this;
  }
  /**
   * Add onTaskSuspend callbacks for the walker inside the runner.
   * @param  {...onTaskSuspend} onTaskSuspend the callbacks invoked when a task suspended
   * @returns {Builder} this builder
   */
  addOnTaskSuspend(...onTaskSuspend) {
    this.wBuilder.addOnTaskSuspend(...onTaskSuspend);
    return this;
  }
  /**
   * Add onTaskSleep callbacks for the walker inside the runner.
   * @param  {...onTaskSleep} onTaskSleep the callbacks invoked when a task went to sleep
   * @returns {Builder} this builder
   * @since 0.3
   */
  addOnTaskSleep(...onTaskSleep) {
    this.wBuilder.addOnTaskSleep(...onTaskSleep);
    return this;
  }
  /**
   * Set enable action tracing or not for the walker inside the runner.
   * @param {boolean} enableActionTracing enable action tracing
   * @returns {Builder} this builder
   */
  setEnableActionTracing(enableActionTracing) {
    this.wBuilder.setEnableActionTracing(enableActionTracing);
    return this;
  }
  /**
   * Build the runner.
   * @returns {WalkerRunner} the runner
   */
  build() {
    return new WalkerRunner(this.wBuilder.build(), this.amount);
  }
  /**
   * Get a new builder.
   * @returns {Builder} a new builder
   */
  static get() {
    return new Builder();
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Builder;