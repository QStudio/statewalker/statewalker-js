/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The core module of StateWalker to define a walker.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 * @since 0.3 refactor the builder to walker-builder.js
 */

/**
 * Module dependencies.
 * @private
 */
/** @typedef {import('./task')} Task */
const Task = require('./task');
/** 
 * @typedef {import('./state')} State
 * @typedef {import('./state').Result} Result
 * @typedef {(task: Task) => Result} onState
 * @typedef {(task: Task) => void} onEnterState
 * @typedef {(task: Task) => void} onExitState
 */
const State = require('./state');
/** 
 * @typedef {import('./transition')} Transition
 * @typedef {(task: Task) => void} onTransition
 */
const Transition = require('./transition');

/**
 * The walker executing tasks based on the predined states and transitions.
 * The task will be executed from state to state till completed.
 * @typedef {(task: Task) => void} onTaskComplete
 * @typedef {(task: Task) => void} onTaskError
 * @typedef {(task: Task) => void} onTaskSuspend
 * @typedef {(task: Task) => void} onTaskSleep
 * @public
 */
class Walker {
  /**
   * Create a walker.
   * @param {boolean} enableActionTracing enable action tracing
   * @package
   */
  constructor(enableActionTracing) {
    /**
     * K is the state code, V is the state.
     * @type {Map<string, State>}
     */
    this.states = new Map();
    /**
     * K is the state code, V is the transition map which K is transition code and V is the transition.
     * @type {Map<string, Map<string, Transition>>}
     */
    this.transitions = new Map();
    /**
     * Callback funtions invoking when a task completed.
     * @type {onTaskComplete[]}
     * @since 0.2
     */
    this.onTaskComplete = [];
    /**
     * Callback funtions invoking when a task raised error.
     * @type {onTaskError[]}
     * @since 0.2
     */
    this.onTaskError = [];
    /**
     * Callback funtions invoking when a task suspended.
     * @type {onTaskSuspend[]}
     * @since 0.2
     */
    this.onTaskSuspend = [];
    /**
     * Callback funtions invoking when a task went to sleep.
     * @type {onTaskSleep[]}
     * @since 0.3
     */
    this.onTaskSleep = [];
    /**
     * The task queue.
     * @type {Task[]}
     * @private
     */
    this._tasks = [];
    /**
     * The tasks are sleeping.
     * @type {Task[]}
     * @since 0.3
     */
    this._sleepingTasks = [];
    /**
     * The runner to run life cycle.
     * @private
     */
    this.runner = enableActionTracing ? new TracedLifeCycleRunner() : new LifeCycleRunner();
  }
  /**
   * Get all tasks.
   * @returns {Task[]} task list
   */
  get tasks() {
    return this._tasks.concat(this._sleepingTasks);
  }
  /**
   * Has task or not.
   * @returns {boolean} true if tasks not completed.
   */
  hasTask() {
    return this._tasks.length > 0 || this._sleepingTasks.length > 0;
  }
  /**
   * Add a new task by content and initial state.
   * @param {*} content the content of the task
   * @param {string} state the initial state of the task
   * @returns {Task} the task added
   */
  addTask(content, state) {
    let t = new Task(content, state);
    this._tasks.push(t);
    return t;
  }
  /**
   * Resume a suspended or error task.
   * @param {Task} task a suspended or error task.
   * @returns {boolean} resume success or not
   */
  resumeTask(task) {
    if ((task.status === Task.Status.SUSPENDED || task.status === Task.Status.ERROR) && !this._tasks.includes(task)) {
      this._tasks.push(task)
      return true;
    }
    return false;
  }
  /**
   * Do a task.
   * This method performs the task by transit it's state to next state.
   * @returns {Task} empty if no task.
   */
  doTask() {
    this.wakeTasks();
    let t = this._tasks.shift();
    if (!t) {
      return;
    }
    try {
      this.execute(t);
    } catch (ex) {
      t.executionError(ex);
      run(this.onTaskError, t);
    }
    return t;
  }
  /**
   * Get state.
   * @param {string} code the state code
   * @returns {State} the state of this walker
   * @private
   */
  getState(code) {
    let state = this.states.get(code);
    if (!state) {
      throw new StateNotFoundError(code);
    }
    return state;
  }
  /**
   * Get transition.
   * @param {string} state current state code
   * @param {string} transition the next transition code
   * @returns {Transition} the transition of this walker
   * @private
   * @since 0.4 redesign the signature
   */
  getTransition(state, transition) {
    if (!transition) {
      return;
    }
    let t = this.transitions.has(state) ? this.transitions.get(state).get(transition) : null;
    if (!t) {
      throw new TransitionNotFoundError(transition)
    }
    return t;
  }
  /**
   * Get next state.
   * @param {string} currentState current state code
   * @param {Transition} transition the transition
   * @returns {State} next state
   * @private
   */
  getNextState(currentState, transition) {
    if (!transition) {
      return this.getState(currentState);
    }
    return this.getState(transition.toState)
  }
  /**
   * Execute a task.
   * @param {Task} task the task
   * @private
   */
  execute(task) {
    task.executionStart();
    let currentState = this.getState(task.state);
    let transition = this.getTransition(task.state, task.transition);
    let nextState = this.getNextState(task.state, transition);
    let result = this.runner.run(currentState, transition, nextState, task);
    switch (result.feedback) {
      case State.Feedback.CONTINUE:
        this.processContinue(task, nextState.code, result.exitCode);
        break;
      case State.Feedback.SUSPEND:
        this.processSuspend(task, nextState.code);
        break;
      case State.Feedback.SLEEP:
        this.processSleep(task, nextState.code, result.sleepTime);
        break;
      case State.Feedback.COMPLETE:
      default:
        this.processComplete(task, nextState.code);
        break;
    }
  }
  /**
   * Process the continued task.
   * @param {Task} task the task
   * @param {string} currentState the code of current state
   * @param {string=} nextTransition the code of next transition
   * @private
   * @since 0.4 redesign the signature
   * @since 0.3
   */
  processContinue(task, currentState, nextTransition) {
    task.executionEnd(currentState, nextTransition);
    this._tasks.push(task);
  }
  /**
   * Process the suspended task.
   * @param {Task} task the task
   * @param {string} currentState the code of current state
   * @private
   * @since 0.3
   */
  processSuspend(task, currentState) {
    task.executionSuspend(currentState);
    run(this.onTaskSuspend, task);
  }
  /**
   * Process the task went to sleep.
   * @param {Task} task the task
   * @param {string} currentState the code of current state
   * @param {number} sleepTime sleep time in milliseconds
   * @private
   * @since 0.3
   */
  processSleep(task, currentState, sleepTime) {
    task.executionSleep(currentState, sleepTime);
    this._sleepingTasks.push(task);
    run(this.onTaskSleep, task);
  }
  /**
   * Process the completed task.
   * @param {Task} task the task
   * @param {string} currentState the code of current state
   * @private
   * @since 0.3
   */
  processComplete(task, currentState) {
    task.executionComplete(currentState);
    run(this.onTaskComplete, task);
  }
  /**
   * Wake up tasks.
   * @private
   * @since 0.3
   */
  wakeTasks() {
    const now = Date.now();
    let s = this._sleepingTasks[0];
    while (s && now >= s.wakeupTime) {
      this._tasks.push(this._sleepingTasks.shift());
      s = this._sleepingTasks[0];
    }
  }
}

/**
 * The based error of StateWalker.
 * @extends Error
 * @package
 */
class BaseError extends Error {
  /**
   * Create an error.
   * @param {*} message the error message
   * @param {*} name the name of this error.
   */
  constructor(name, message) {
    super(message);
    this.name = name;
  }
}

/**
 * Thrown when walker can't find the state.
 * @extends BaseError
 * @public
 */
class StateNotFoundError extends BaseError {
  /**
   * Create an error.
   * @param {string} stateCode the state code
   * @override
   */
  constructor(stateCode) {
    super('StateNotFoundError', `State[${stateCode}] is not found`);
  }
}

/**
 * Thrown when walker can't find the transition.
 * @extends BaseError
 * @public
 */
class TransitionNotFoundError extends BaseError {
  /**
   * Create an error.
   * @param {string} tranistionCode the transition code
   * @override
   */
  constructor(tranistionCode) {
    super('TransitionNotFoundError', `Transition[${tranistionCode}] is not found`);
  }
}

/**
 * Run life cycle without action tracing.
 * @private
 */
class LifeCycleRunner {
  /**
   * Run the life cycle.
   * @param {State} currentState current state of the task
   * @param {Transition} transition upcoming transition of the task
   * @param {State} nextState next state of the task
   * @param {Task} task the task
   * @returns {Result} the result of onState
   */
  run(currentState, transition, nextState, task) {
    if (transition) {
      currentState.onExitState(task);
      transition.onTransition(task);
      nextState.onEnterState(task);
    }
    return nextState.onState(task);
  }
}

/**
 * Run life cycle with action tracing.
 * @extends LifeCycleRunner
 * @private
 */
class TracedLifeCycleRunner extends LifeCycleRunner {
  /**
   * Run the life cycle.
   * @param {State} currentState current state of the task
   * @param {Transition=} transition upcoming transition of the task
   * @param {State} nextState next state of the task
   * @param {Task} task the task
   * @returns {Result} the result of onState
   * @override
   */
  run(currentState, transition, nextState, task) {
    if (transition) {
      this.executeTracedLifeCycle(currentState, "onExitState", task);
      this.executeTracedLifeCycle(transition, "onTransition", task);
      this.executeTracedLifeCycle(nextState, "onEnterState", task);
    }
    return this.executeTracedLifeCycle(nextState, "onState", task);
  }
  /**
   * Execute life cycle method with tracing.
   * @param {State|Transition} target target state or transition
   * @param {string} methodName which method to run
   * @param {Task} task the task
   * @returns {Result} result of target method
   * @private
   */
  executeTracedLifeCycle(target, methodName, task) {
    let result;
    task.actionStart(methodName, target.code);
    switch (methodName) {
      case 'onExitState':
        target.onExitState(task);
        break;
      case 'onTransition':
        target.onTransition(task);
        break;
      case 'onEnterState':
        target.onEnterState(task);
        break;
      case 'onState':
        result = target.onState(task);
        break;
    }
    task.actionEnd();
    return result;
  }
}

/**
 * Run functions.
 * @param {Function[]} functions the function array
 * @param {*} argument 
 * @private
 * @since 0.2
 */
function run(functions, argument) {
  for (let f of functions) {
    f(argument);
  }
}

/**
 * Module exports.
 * @public
 */
module.exports = Walker;
module.exports.StateNotFoundError = StateNotFoundError;
module.exports.TransitionNotFoundError = TransitionNotFoundError;