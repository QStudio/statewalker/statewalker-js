/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * Sample Code: TrafficLightController.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */

const { State, WalkerRunner } = require('../../index')

class Light {
  constructor(id) {
    this.id = id;
    this.color = undefined;
    this.duration = undefined;
    this.startTime = undefined;
    this.on = true;
  }
  get busy() {
    if (this.startTime && this.duration) {
      return (Date.now() - this.startTime) < (this.duration * 1000);
    }
    return false;
  }
  info() {
    return `Light-${this.id}: ${this.color} => ${this.duration}`;
  }
  show(color, duration) {
    if (!this.busy) {
      this.color = color;
      this.duration = duration;
      this.startTime = Date.now();
      console.log(this.info());
    }
  }
  turnOff() {
    this.on = false;
    this.color = "BLACK";
    this.duration = null;
    this.startTime = null;
  }
}

class Red extends State {
  onState(task) {
    if (task.content.color !== 'RED') {
      task.content.show('RED', 10);
      return State.Result.sleep(10000);
    }
    if (task.content.busy) {
      return State.Result.stay();
    }
    return State.Result.exit('NEXT');
  }
}

class Yellow extends State {
  onState(task) {
    if (task.content.color !== 'YELLOW') {
      task.content.show('YELLOW', 3);
      return State.Result.sleep(3000);
    }
    if (task.content.busy) {
      return State.Result.stay();
    }
    return State.Result.exit('NEXT');
  }
}

class Green extends State {
  onState(task) {
    if (task.content.color !== 'GREEN') {
      task.content.show('GREEN', 7)
      return State.Result.sleep(7000);
    }
    if (task.content.busy) {
      return State.Result.stay();
    }
    return State.Result.exit('NEXT');
  }
}

class Off extends State {
  onState(task) {
    task.content.turnOff();
    return State.Result.complete();
  }
}

class TrafficLightController {
  constructor() {
    this.runner = WalkerRunner.Builder.get()
      .addStates(new Red(), new Yellow(), new Green(), new Off())
      .addTransition('NEXT', 'Red', 'Yellow')
      .addTransition('NEXT', 'Yellow', 'Green')
      .addTransition('NEXT', 'Green', 'Off')
      .build();
    this.lights = [];
  }
  addLight(light) {
    this.lights.push(light);
    this.runner.assignTask(light, 'Red');
  }
  start() {
    this.runner.start();
  }
  stop() {
    this.runner.stop();
  }
  isBusy() {
    return this.runner.isBusy();
  }
}

let controller = new TrafficLightController();
controller.addLight(new Light(1));
controller.addLight(new Light(2));
controller.addLight(new Light(3));
controller.start();
let wait = () => {
  if (controller.isBusy()) {
    console.log("WAIT...");
    setTimeout(wait, 1000);
  } else {
    controller.stop();
    for (const light of controller.lights) {
      console.log(light.info());
    }
    console.log("COMPLETED");
  }
}
wait();