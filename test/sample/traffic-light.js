/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * Sample Code: TrafficLight.
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */

const { State, Transition, Task, Walker } = require('../../index')

class Red extends State {
  onState(task) {
    task.content.color = 'Red';
    task.content.duration = 60;
    return State.Result.exit('NEXT');
  }
}

class Yellow extends State {
  onState(task) {
    task.content.color = 'Yellow';
    task.content.duration = 5;
    return State.Result.exit('NEXT');
  }
}

class Green extends State {
  onState(task) {
    task.content.color = 'Green';
    task.content.duration = 30;
    return State.Result.exit('NEXT');
  }
}

const r2y = Transition.Builder.get().setFromState('Red').setToState('Yellow').build();
const y2g = Transition.Builder.get().setFromState('Yellow').setToState('Green').build();
const g2r = Transition.Builder.get().setFromState('Green').setToState('Red').build();

let walker = Walker.Builder.get()
  .addStates(new Red(), new Yellow(), new Green())
  .addTransition('NEXT', 'Red', 'Yellow')
  .addTransition('NEXT', 'Yellow', 'Green')
  .addTransition('NEXT', 'Green', 'Red')
  .build();
// Red
let task = walker.addTask({ color: null, duration: 0 }, 'Red');
walker.doTask();
console.log(JSON.stringify(task.content));
console.log(`======= AFTER ${task.content.duration} SECONDS =======`);
// Yellow
walker.doTask();
console.log(JSON.stringify(task.content));
console.log(`======= AFTER ${task.content.duration} SECONDS =======`);
// Green
walker.doTask();
console.log(JSON.stringify(task.content));
console.log(`======= AFTER ${task.content.duration} SECONDS =======`);
// Red
walker.doTask();
console.log(JSON.stringify(task.content));