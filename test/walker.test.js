/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The test cases for walker.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3
 */

const { State, Transition, Task, Walker } = require('../index')

test('Walker: no task', () => {
  let walker = Walker.Builder.get().build();
  expect(walker.doTask()).toBeUndefined();
});

test('Walker: add task', () => {
  let walker = Walker.Builder.get().build();
  walker.addTask(null, "A");
  expect(walker.tasks.length).toBe(1);
  walker.addTask(null, "A");
  expect(walker.tasks.length).toBe(2);
});

test('Walker: no current state', () => {
  let walker = Walker.Builder.get().build();
  let task = walker.addTask(null, "A");
  walker.doTask();
  expect(task.status).toBe(Task.Status.ERROR);
  expect(task.errorCause).toBeInstanceOf(Walker.StateNotFoundError);
});

test('Walker: no next state', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => State.Result.exit('DONE'))
    .build();
  let t1 = Transition.Builder.get()
    .setCode('DONE')
    .setFromState('A').setToState('B')
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .addTransitions(t1)
    .build();
  let task = walker.addTask(null, 'A');
  walker.doTask();
  walker.doTask();
  expect(task.status).toBe(Task.Status.ERROR);
  expect(task.errorCause).toBeInstanceOf(Walker.StateNotFoundError);
});

test('Walker: no transition', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => State.Result.exit('DONE'))
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();
  let task = walker.addTask(null, 'A');
  walker.doTask();
  walker.doTask();
  expect(task.status).toBe(Task.Status.ERROR);
  expect(task.errorCause).toBeInstanceOf(Walker.TransitionNotFoundError);
});

test('Walker: resume a processing task', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => State.Result.stay())
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();
  let task = walker.addTask(null, 'A');
  walker.doTask();
  expect(task.status).toBe(Task.Status.PROCESSING);
  expect(walker.resumeTask(task)).toBe(false);
});

test('Walker: resume an error task', () => {
  let walker = Walker.Builder.get().build();
  let task = walker.addTask(null, 'A');
  walker.doTask();
  expect(task.status).toBe(Task.Status.ERROR);
  expect(walker.tasks.length).toBe(0);
  expect(walker.resumeTask(task)).toBe(true);
  expect(walker.tasks.length).toBe(1);
  expect(walker.doTask()).toBe(task);
});

test('Walker: resume a suspended task', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => State.Result.suspend())
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();

  let task = walker.addTask(null, 'A');
  walker.doTask();
  expect(task.status).toBe(Task.Status.SUSPENDED);
  expect(walker.tasks.length).toBe(0);
  expect(walker.resumeTask(task)).toBe(true);
  expect(walker.resumeTask(task)).toBe(false);
  expect(walker.tasks.length).toBe(1);
  expect(walker.doTask()).toBe(task);
  expect(walker.hasTask()).toBe(false);
});

test('Walker: resume a completed task', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();
  let task = walker.addTask(null, "A");
  walker.doTask();
  expect(task.status).toBe(Task.Status.COMPLETED);
  expect(walker.tasks.length).toBe(0);
  expect(walker.resumeTask(task)).toBe(false);
  expect(walker.tasks.length).toBe(0);
});

test('Walker: previous state', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => State.Result.stay())
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();
  let task = walker.addTask(null, 'A');
  walker.doTask();
  expect(task.previousState).toBeUndefined();
  walker.doTask();
  expect(task.previousState).toBe('A');
});

test('Walker: task sleeping', () => {
  let s1 = State.Builder.get()
    .setCode('A')
    .setOnState(t => {
      t.content.count++;
      if (t.content.count < 2) {
        return State.Result.sleep(-1000);
      }
      if (t.content.count < 3) {
        return State.Result.sleep(1000);
      }
      return State.Result.complete();
    })
    .build();
  let walker = Walker.Builder.get()
    .addStates(s1)
    .build();
  let t1 = walker.addTask({ count: 0 }, 'A');
  let t2 = walker.addTask({ count: 0 }, 'A');
  while (t1.status !== Task.Status.COMPLETED) {
    walker.doTask();
  }
  expect(walker.hasTask()).toBeTruthy();
  expect(t1.content.count).toBe(3);
  expect(t2.status).toBe(Task.Status.SLEEPING);
});

test('Walker: state and transition by implementation', () => {
  let walker = Walker.Builder.get()
    .addStates(new StateA(), new StateB())
    .addTransitions(new Transition1(), new Transition2())
    .setEnableActionTracing(true)
    .build();
  let task = walker.addTask({ count: 0 }, 'StateA');
  while (walker.hasTask()) {
    walker.doTask();
    for (let a of task.actions) {
      expect(a.startTime).toBeLessThanOrEqual(a.endTime);
    }
  }
  expect(task.status).toBe(Task.Status.COMPLETED);
  expect(task.state).toBe('StateB');
  expect(task.content.count).toBe(4);
});

test('Walker: state and transition declaration', () => {
  let walker = Walker.Builder.get()
    .addState('StateA',
      t => {
        console.log(`StateA | ${t.content.count}`)
        t.content.count++;
        if (t.content.count < 2) {
          return State.Result.stay();
        }
        return State.Result.exit('DONE');
      },
      t => console.log(`After Exit StateA | ${t.content.count}`),
      t => console.log(`Before Enter StateA | ${t.content.count}`)
    )
    .addState('StateB',
      t => {
        console.log(`StateB | ${t.content.count}`)
        if (t.content.count > 3) {
          return State.Result.complete();
        }
        t.content.count++;
        return State.Result.exit('DONE');
      }
    )
    .addTransition('DONE', 'StateA', 'StateB', t => console.log(`Transtion1 | ${t.content.count}`))
    .addTransition('DONE', 'StateB', 'StateA')
    .build();
  let task = walker.addTask({ count: 0 }, 'StateA');
  while (walker.hasTask()) {
    walker.doTask();
  }
  expect(task.status).toBe(Task.Status.COMPLETED);
  expect(task.state).toBe('StateB');
  expect(task.content.count).toBe(4);
});

test('Walker: task callbacks', () => {
  var onTaskComplete = t => { t.content.value = 1 };
  var onTaskSuspend = t => { t.content.value = 2 };
  var onTaskSleep = t => { t.content.value = 3 };
  var onTaskError = t => { t.content.value = 4 };
  // complete
  let walker = Walker.Builder.get()
    .addState('A', t => State.Result.complete())
    .addState('B', t => State.Result.suspend())
    .addState('C', t => State.Result.sleep(5000))
    .addOnTaskComplete(onTaskComplete, onTaskComplete)
    .addOnTaskSuspend(onTaskSuspend, onTaskSuspend)
    .addOnTaskSleep(onTaskSleep, onTaskSleep)
    .addOnTaskError(onTaskError, onTaskError)
    .build();
  // Complete
  let task = walker.addTask({}, 'A');
  walker.doTask();
  expect(task.content.value).toBe(1);
  // Suspend
  task = walker.addTask({}, 'B');
  walker.doTask();
  expect(task.content.value).toBe(2);
  // Sleep
  task = walker.addTask({}, 'C');
  walker.doTask();
  expect(task.content.value).toBe(3);
  // Error
  task = walker.addTask({}, 'D');
  walker.doTask();
  expect(task.content.value).toBe(4);
});

class StateA extends State {
  constructor() {
    super('StateA');
  }
  onState(task) {
    console.log(`StateA | ${task.content.count}`)
    task.content.count++;
    if (task.content.count < 2) {
      return State.Result.stay();
    }
    return State.Result.exit('DONE');
  }
  onEnterState(task) {
    console.log(`Before Enter StateA | ${task.content.count}`)
  }
  onExitState(task) {
    console.log(`After Exit StateA | ${task.content.count}`);
  }
}

class StateB extends State {
  onState(task) {
    console.log(`StateB | ${task.content.count}`)
    if (task.content.count > 3) {
      return State.Result.complete();
    }
    task.content.count++;
    return State.Result.exit('DONE');
  }
}

class Transition1 extends Transition {
  constructor() {
    super('StateA', 'StateB', 'DONE');
  }
  onTransition(task) {
    console.log(`Transtion1 | ${task.content.count}`)
  }
}

class Transition2 extends Transition {
  constructor() {
    super('StateB', 'StateA', 'DONE');
  }
}
