/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The test cases for transition.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3
 */

const { Transition } = require('../index')

test('Transition.Builder', () => {
  let t1 = Transition.Builder.get().build();
  expect(t1.code).toBe('Transition');
  let t2 = Transition.Builder.get()
    .setFromState('A')
    .build();
  expect(t2.code).toBe('Transition');
  let t3 = Transition.Builder.get()
    .setFromState('A')
    .setToState('B')
    .build();
  expect(t3.code).toBe('Transition');
  let t4 = Transition.Builder.get()
    .setFromState('A')
    .setToState('B')
    .setCode('C')
    .build();
  expect(t4.code).toBe('C');
});
