/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The test cases for walker-runner.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3
 */

const { State, Transition, Task, WalkerRunner } = require('../index')

test('WalkerRunner: runner', () => {
  jest.useFakeTimers();
  var onTaskComplete = t => {
    t.content.value = 1;
    expect(t.actions.length > 0).toBe(true);
  };
  var onTaskError = t => { t.content.value = 2 };
  var onTaskSuspend = t => { t.content.value = 3 };
  var onTaskSleep = t => { t.content.value = 4 };

  let runner = WalkerRunner.Builder.get()
    .addStates(new StateA())
    .addState('B', t => {
      console.log(`B | ${task.content.count}`)
      return State.Result.complete();
    })
    .addTransitions(new Transition1())
    .addTransition('Transition2', 'B', 'A')
    .addOnTaskComplete(onTaskComplete)
    .addOnTaskError(onTaskError)
    .addOnTaskSuspend(onTaskSuspend)
    .addOnTaskSleep(onTaskSleep)
    .setEnableActionTracing(true)
    .build();

  expect(runner.hasTask()).toBe(false);
  expect(runner.isRunning()).toBe(false);
  expect(runner.isBusy()).toBe(false);

  expect(runner.start()).toBe(true);
  expect(runner.start()).toBe(false);
  expect(runner.isRunning()).toBe(true);
  expect(runner.isBusy()).toBe(false);

  let task = runner.assignTask({ count: 0 }, 'A');
  expect(runner.hasTask()).toBe(true);
  expect(runner.isBusy()).toBe(true);

  setImmediate(() => {
    // SLEEP
    expect(runner.isBusy()).toBe(true);
    expect(runner.isRunning()).toBe(true);
    expect(runner.hasTask()).toBe(true);
    expect(task.status).toBe(Task.Status.SLEEPING);
  });
  jest.runOnlyPendingTimers();
  setTimeout(() => {
    // WAKE
    expect(task.status).toBe(Task.Status.PROCESSING);
    expect(task.content.value).toBe(4);
  }, 1000);
  jest.runOnlyPendingTimers();
  setImmediate(() => {
    // COMPLETE
    expect(task.status).toBe(Task.Status.COMPLETED);
  });
  jest.runOnlyPendingTimers();
  setImmediate(() => {
    // STOP
    expect(runner.stop()).toBe(true);
    expect(runner.stop()).toBe(false);
    expect(runner.isRunning()).toBe(true);
  });
  jest.runOnlyPendingTimers();
  setTimeout(() => {
    expect(runner.isRunning()).toBe(false);
  }, 1000);
  jest.runOnlyPendingTimers();
});

class StateA extends State {
  constructor() {
    super('A');
  }
  onState(task) {
    console.log(`A | ${task.content.count}`)
    if (task.content.count++ < 1) {
      return State.Result.sleep(1);
    }
    return State.Result.exit('Transition1');
  }
  onEnterState(task) {
    console.log(`Before Enter A | ${task.content.count}`)
  }
  onExitState(task) {
    console.log(`After Exit A | ${task.content.count}`);
  }
}

class Transition1 extends Transition {
  constructor() {
    super('A', 'B');
  }
  onTransition(task) {
    console.log(`Transtion1 | ${task.content.count}`)
  }
}
