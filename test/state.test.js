/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The test cases for state.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3
 */

const { State } = require('../index')

test('State.Builder', () => {
  let s1 = State.Builder.get().build();
  expect(s1.code).toBe('State');
  let s2 = State.Builder.get()
    .setCode('A')
    .build();
  expect(s2.code).toBe('A');
  let s3 = State.Builder.get()
    .setCode('A')
    .setOnState((t) => State.Result.complete())
    .build();
  expect(s3.code).toBe('A');
});
