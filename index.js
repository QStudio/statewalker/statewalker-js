/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
'use strict';

/**
 * The index module of StateWalker.
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.1
 */

module.exports.State = require('./lib/state');
/**
 * @since 0.3
 */
module.exports.State.Builder = require('./lib/state-builder');
module.exports.Transition = require('./lib/transition');
/**
 * @since 0.3
 */
module.exports.Transition.Builder = require('./lib/transition-builder');
module.exports.Task = require('./lib/task');
module.exports.Walker = require('./lib/walker');
/**
 * @since 0.3
 */
module.exports.Walker.Builder = require('./lib/walker-builder');
module.exports.WalkerRunner = require('./lib/walker-runner');
/**
 * @since 0.3
 */
module.exports.WalkerRunner.Builder = require('./lib/walker-runner-builder');