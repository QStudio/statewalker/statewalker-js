# StateWalker
StateWalker is an easy to use flow engine based on Finite State Machine(FSM) for Node.js.  
The purpose of this project is making a flow system can be designed and implemented easily, and make sure the FSM model can be executed with high performance. 

---
## Installation
1. Setup repository registry in your npm project
```
echo @QStudio:registry=https://gitlab.com/api/v4/packages/npm >> .npmrc
```
2. Install StateWalker-JS in your npm project
```
npm i @QStudio/statewalker-js
```

---
## Finite State Machine Concept of StateWalker
StateWalker provides `State` and `Transition` to build a FSM model which can be described as follows:
```
State(S) -- Transition(T) -> State(S')
```
Each `State` and `Transition` can implement different behavior on demand.

A FSM model is desiged to accomplish some `Task` which carries specific `Content` and needs an engine to run it.  
So, we designed `Walker` which execute tasks from state to state and `WalkerRunner` to run multiple walkers automatically and asynchronously.

---
### Define a State
#### By Implementation
A `State` can be created by extends `State`.  
There are three life cycle APIs:
 - `onEnterState` - before enter the state
 - `onState` - on the state
 - `onExitState` - before exit the state
```javascript
class Red extends State {
  onState(task) {
    task.content.color = 'Red';
    task.content.duration = 60;
    return State.Result.next('Yellow');
  }
}
```
#### By Declaration
A `State` can be created by declaration style API.
```javascript
let red = State.Builder.get()
  .setCode('Red')
  .setOnState(t => {
     t.content.color = 'Red';
     t.content.duration = 60;
     return State.Result.next('Yellow');
  })
  .build();
```

#### About State.Result
There are 5 APIs can be used when implements the logic of `onState` task operation.
- `exit(exitCode)` - Ready to transit to next state. This task will be added to task queue automatically.
  - The `exitCode` is the code of the transition which start from this state.
- `stay()` - Stay at the same state. This task will be added to task queue automatically.
- `suspend()` - Suspend and stay on the same state. This task can be resumed manually.
- `complete()` - Task is completed.
- `sleep(sleepTime)` - Sleep for the specific time period.

---
### Define a Transition
#### By Implementation
A `Transition` can be created by extends `Transition`.  
There is optional life cycle api `onTransition` can be implemented.
```javascript
class R2Y extends Transition {
  constructor() {
    super('Red', 'Yellow', 'DONE');
  }
}
```
#### By Declaration
A `Transition` can be created by declaration style API.
```javascript
let r2y = Transition.Builder.get()
  .setCode('DONE')
  .setFromState('Red')
  .setToState('Yellow')
  .build();
```

---
### Build a Walker
If you want to control state transition **by yourself**, you can build a walker and use it manually.

A walker to be build needs providing it's states and transitions.
```javascript
let walker = Walker.Builder.get()
  .addStates(red, yellow, green)
  .addTransitions(r2y, y2g, g2r)
  .build();
```
You can also define a state or transition during building a walker using these APIs.
- `addState(code, onState, onEnterState, onExitState)`
- `addTransition(code, fromState, toState, onTransition)`

#### Add a Task
Add a task to task queue by invoking `addTask`.
```javascript
let task = walker.addTask({ color: null, duration: 0 }, 'Red');
```
Execute first task in the quere by invoking `doTask`.   
`doTask` exeute the life cycle api from the task's current state to next state, then determine what to do next based on the logic implemented.
```javascript
let taskExecuted = walker.doTask();
```

#### About Callbacks
There are 3 types of callback invoked by a walker when task achieved the status.
- `onTaskComplete`
- `onTaskError`
- `onTaskSuspend`
- `onTaskSleep`

To use these callbacks you need provide them when building a walker.
```javascript
let walker = Walker.Builder.get()
  .addStates(red, yellow, green)
  .addTransitions(r2y, y2g, g2r)
  .addOnTaskComplete(cb1, cb2)
  .addOnTaskError(cb3, cb4)
  .addOnTaskSuspend(cb5, cb6)
  .addOnTaskSleep(cb7, cb7)
  .build();
```

#### Trace the State Transition
Tracing state transition can be enable by two steps:
1. Build a action tracing walker
```javascript
let walker = Walker.Builder.get()
    .addStates(s1, s2)
    .addTransitions(t1, t2)
    .setEnableActionTracing(true)
    .build();
```
2. Get the actions after `doTask()`
```javascript
let t = walker.doTask();
t.actions;
```

---
### Build a WalkerRunner
If you want to run state transition **automatically and asynchronously**, you can build a runner and just start it.

To build a runner is just like to build a walker. The `WalkerRunner.Builder` includes all APIs of `Walker.Builder`. 
```javascript
let walker = WalkerRunner.Builder.get()
  .addStates(red, yellow, green)
  .addTransitions(r2y, y2g, g2r)
  .build();
```

#### Use a WalkerRunner
We provide these APIs to use a runner.
- `assignTask(content, state)` - just like `addTask` of `Walker`
- `start()` - start the runner
- `stop()` - stop the runner
- `hasTask()` - the runner has task to do
- `isRunning()` - the runner started and still running
- `isBusy()` - the runner is running and has task to be completed.

---
## Sample Code
Please take a look the samples and test cases at `test/` in this project.
